import 'package:flutter/widgets.dart';

class ScoreProvider extends ChangeNotifier {
  int _score = 0;

  int get score => _score;

  void updateScore(int newScore) {
    _score = newScore;
    notifyListeners();
  }
}
