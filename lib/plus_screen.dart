import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'ChangeNotifier.dart';

class plusScreen extends StatefulWidget {
  const plusScreen({Key? key}) : super(key: key);

  @override
  _PlusScreeState createState() => _PlusScreeState();
}

class _PlusScreeState extends State<plusScreen> {
  int num1 = Random().nextInt(10);
  int num2 = Random().nextInt(10);
  int? userAnswer;
  int answer = 0;
  int score = 0;
  int time = 10;
  TextEditingController controller = TextEditingController();
  Timer? timer;

  void generateQuestion() {
    setState(() {
      num1 = Random().nextInt(10);
      num2 = Random().nextInt(10);
      answer = num1 + num2;
      userAnswer = null;
      controller.clear();
      time = 50;
    });
    startTimer();
  }

  void checkAnswer() {
    if (userAnswer == null) return;

    if (userAnswer == answer) {
      setState(() {
        score++;
        if (score == 5) {
          timer?.cancel();
          Provider.of<ScoreProvider>(context, listen: false).updateScore(score);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => CongratulationsPage(),
            ),
          );
        } else {
          generateQuestion();
        }
      });
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text('Sorry!'),
          content: Text('Incorrect answer. Try again!'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
                startTimer();
              },
              child: Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  void startTimer() {
    timer?.cancel();
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (time == 0) {
          timer.cancel();
          showDialog(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: Text('Time up!'),
              content: Text('Sorry, you ran out of time.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    generateQuestion();
                  },
                  child: Text('OK'),
                ),
              ],
            ),
          );
        } else {
          time--;
        }
      });
    });
  }

  @override
  void initState() {
    super.initState();
    generateQuestion();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 80;
    return Scaffold(
      appBar: AppBar(
        title: Text('Number Plus Game'),
      ),
      body: Container(
        color: Color(0xFF272837),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '$num1 + $num2 = ?',
                style: TextStyle(
                  fontSize: 80,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 40),
              SizedBox(
                width: 400,
                child: TextField(
                  controller: controller,
                  onChanged: (value) => setState(() {
                    if (value.isEmpty) {
                      userAnswer = null;
                    } else {
                      userAnswer = int.tryParse(value);
                    }
                  }),
                  decoration: InputDecoration(
                    hintText: 'Enter your answer',
                    hintStyle: TextStyle(color: Colors.white),
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 10,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    suffixIcon: userAnswer != null
                        ? IconButton(
                            onPressed: () => controller.clear(),
                            icon: Icon(Icons.clear),
                            color: Colors.white,
                          )
                        : null,
                  ),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(height: 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text(
                        'Time left:',
                        style: TextStyle(fontSize: 30, color: Colors.white),
                      ),
                      SizedBox(height: 5),
                      Text(
                        '$time',
                        style: TextStyle(fontSize: 30, color: Colors.white),
                      ),
                    ],
                  ),
                  SizedBox(width: 40), // add some space between the two widgets
                  Column(
                    children: [
                      Text(
                        'Score:',
                        style: TextStyle(fontSize: 30, color: Colors.white),
                      ),
                      SizedBox(height: 5),
                      Text(
                        '$score',
                        style: TextStyle(fontSize: 30, color: Colors.white),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 55),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  circButton(FontAwesomeIcons.star),
                  circButton(FontAwesomeIcons.star),
                  circButton(FontAwesomeIcons.star),
                  circButton(FontAwesomeIcons.star),
                ],
              ),
              SizedBox(height: 55),
              SizedBox(
                height: 60,
                width: 200,
                child: ElevatedButton(
                  onPressed: checkAnswer,
                  child: Text(
                    'Submit',
                    style: TextStyle(fontSize: 30),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Color(0xFF45D280),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CongratulationsPage extends StatelessWidget {
  const CongratulationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Congratulations!'),
      ),
      body: Container(
        color: Color(0xFF272837),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                FontAwesomeIcons.trophy,
                size: 100,
                color: Colors.yellow,
              ),
              SizedBox(height: 40),
              Text(
                'Congratulations!',
                style: TextStyle(
                  fontSize: 50,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 40),
              Text(
                'You have answered 5 questions correctly.',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 40),
              ElevatedButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'OK',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class circButton extends StatelessWidget {
  final IconData iconData;
  const circButton(this.iconData);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      height: 80,
      child: IconButton(
        iconSize: 40,
        onPressed: () {},
        icon: FaIcon(
          iconData,
          color: Colors.white,
        ),
      ),
      decoration: BoxDecoration(
        color: Colors.amber,
        borderRadius: BorderRadius.circular(50),
      ),
    );
  }
}
