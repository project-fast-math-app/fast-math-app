import 'package:flutter/material.dart';
import 'package:flutter_fast_math_app/divide_screen.dart';
import 'package:flutter_fast_math_app/minus_screen.dart';
import 'package:flutter_fast_math_app/multiplied_screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'plus_screen.dart';

class Mymenu extends StatelessWidget {
  const Mymenu({super.key});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 80;
    return Container(
      color: Color(0xFF272837),
      child: Center(
          child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 40, 0, 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                Text(
                  'Fast Math ',
                  style: TextStyle(
                    fontFamily: 'Manrope',
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.none,
                    fontSize: 36,
                    color: Colors.white,
                  ),
                ),
                Text(
                  'Puzzle',
                  style: TextStyle(
                    fontFamily: 'Manrope',
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.none,
                    fontSize: 36,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                circButton(FontAwesomeIcons.medal),
                circButton(FontAwesomeIcons.medal),
                circButton(FontAwesomeIcons.medal),
                circButton(FontAwesomeIcons.medal),
              ],
            ),
            Wrap(
              runSpacing: 16,
              children: [
                modeButton(
                  'Easy Mode',
                  'Plus level',
                  FontAwesomeIcons.trophy,
                  Color(0xFF45D280),
                  width,
                  () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) => plusScreen()),
                    );
                  },
                ),
                modeButton(
                  'Normal Mode',
                  'Minus level',
                  FontAwesomeIcons.trophy,
                  Color(0xFF2F80Ed),
                  width,
                  () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) => MinusScreen()),
                    );
                  },
                ),
                modeButton(
                  'Hard Mode',
                  'Multiplied level',
                  FontAwesomeIcons.trophy,
                  Color(0xFFFF8306),
                  width,
                  () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              MultipliedScreen()),
                    );
                  },
                ),
                modeButton(
                  'Very Hard Mode',
                  'Divide level',
                  FontAwesomeIcons.trophy,
                  Color(0xFFDF1D5A),
                  width,
                  () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) => DivideScreen()),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      )),
    );
  }

  Padding circButton(IconData icon) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0),
      child: RawMaterialButton(
        onPressed: () {},
        fillColor: Colors.white,
        shape: CircleBorder(),
        constraints: BoxConstraints(minHeight: 35, minWidth: 35),
        child: FaIcon(
          icon,
          size: 22,
          color: Color(0xFF2F3041),
        ),
      ),
    );
  }

  Widget modeButton(
    String title,
    String subtitle,
    IconData icon,
    Color color,
    double width,
    VoidCallback onTap,
  ) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(16)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 22.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                      fontFamily: 'Manrope',
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0),
                    child: Text(
                      subtitle,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                        fontFamily: 'Manrope',
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
              child: FaIcon(
                icon,
                size: 45,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }
}
